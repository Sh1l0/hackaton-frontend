import React, { Component } from 'react';
import PropTypes from 'prop-types';


export default class Level extends Component {
  constructor(props) {
    super(props);
    this.state = props;
  }

  parseWords(words, letters) {
    let flag = false;
    for(let i = 0, j = 0; i < letters.length; i++, j++) {
      if((words.length === 0) && (flag)) {
        continue;
      }
      if(words.length === 0) {
        const div = document.createElement('div');
        div.className = "letterName";
        div.innerHTML = "Пока что ни одного слова не изучено";
        document.getElementById("title").appendChild(div);
        flag = true;
        continue;
      }
      if((words[i] === "") ||(words[i] === " ")) {
        continue;
      }
      if(words[i] == undefined) continue;
      let a = words[j].split(" ");
      if(a.length != 0) {
        const div = document.createElement("div");
        div.className = "letterName";
        div.id = i;
        div.innerHTML = letters[i].toUpperCase();
        let str = "";
        for(let each of a) {
          str += `${each} `;
        }
        document.getElementById("title").appendChild(div);
        const additionalDiv = document.createElement("div");
        additionalDiv.className = "words";
        additionalDiv.innerHTML = str;
        document.getElementById(i).appendChild(additionalDiv);
      }
    }
  }

  componentDidMount() {
    let words = []; //здесь будет запрос
    words[0] = "азбука алфавит арбуз асфальт";
    words[1] = "бабушка бегемот бублик";
    words[2] = "";
    words[3] = "";
    let letters = ["а", "б", "в", "г", "д",
    "е", "ё", "ж", "з", "и", "й", "к", "л",
    "м", "н", "о", "п", "р", "с", "т", "у",
    "ф", "х", "ц", "ч", "ш", "щ", "ъ", "ы",
    "ь", "э", "ю", "я"];

    this.parseWords(words, letters);
  }

  render() {
    return (
      <div className="vocabulary">
        <div className="title" id="title">Словарь</div>
      </div>
    )
  }
}


Level.propTypes = {

};

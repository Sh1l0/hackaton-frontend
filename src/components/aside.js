import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class Aside extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isUpperActive: true,
      click: this.props.click,
      upperClass: 'upper active',
      lowerClass: 'lower'
    }
  }

  render() {
    return (
      <div className="nav">
        <ul className="tabs">
          <li className={this.state.upperClass} onClick={() => {
            this.setState({upperClass: 'upper active', lowerClass: 'lower'});
            this.state.click(true)}}>
            Успеваемость <br />по категориям
          </li>
          <li id="stats" className={this.state.lowerClass} onClick={() => {
            this.setState({lowerClass: 'lower active', upperClass: 'upper'});
            this.state.click(false)}}>
            Словарь
          </li>
        </ul>
      </div>
    )
  }
}

Aside.propTypes = {
  isUpperActive: PropTypes.bool,
  click: PropTypes.func,
}

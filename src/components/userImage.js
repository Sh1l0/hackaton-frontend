import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class UserImage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      path: this.props.path
    }
  }

  componentDidMount() {
    fetch(this.state.path).then((res) => {
        if(!res.ok) {
          this.setState({path: 'src/img/user.png'})
          console.log(1);
          return;
        }
    });

  }

  render() {
    return (
      <div className="image">
        <img src={this.state.path} width="200" alt="[" />
      </div>
    )
  }
}


UserImage.propTypes = {
  path: PropTypes.string
};

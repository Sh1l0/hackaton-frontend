import React, { Component } from 'react';
import PropTypes from 'prop-types';


export default class Level extends Component {
  constructor(props) {
    super(props);
    this.state = props;
  }


  delay = ms => new Promise(resolve => setTimeout(resolve, ms));

  levelGrow = (points, i) => {
        if(i === points) {
          return;
        }
        document.getElementById("hiddenLvl").style.width = `${600 - 6*i}px`;
        setTimeout(() => {this.levelGrow(points, i+1)}, 10);
  }
  componentDidMount() {
    this.levelGrow(this.state.points, 0)
  }
  render() {
    return (
      <div className="levelArea">
        <div className="prev">{this.state.prevLvl}</div>
        <div className="levelHidden" id="hiddenLvl"></div>
        <div className="level"></div>
        <div className="next">{this.state.prevLvl + 1}</div>
      </div>
    )
  }
}


Level.propTypes = {

};

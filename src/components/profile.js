import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Level from './level';
import Info from './info';

export default class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = props;
  //  console.log(this.state);
  }
  render() {
    return (
      <div className="profileInfo">
        <div className="name">{this.state.name}</div>
        <div className="levelText">Уровень сложности</div>
        <Level points={89} prevLvl={1} />
        <div className="additionalInfo">
          <Info path='src/img/tochnost.png' alt="Точность" data={this.state.tochnost}>Точность</Info>
          <Info path='src/img/clock.png' alt="Часы" data={this.state.time}>Время</Info>
          <Info path='src/img/cards.png' alt="Карты" data={this.state.time}>Количество карточек</Info>
        </div>
      </div>
    )
  }
}


Profile.propTypes = {

};

import React, { Component } from 'react';
import Circle from './circle';
import PropTypes from 'prop-types';


export default class Level extends Component {
  constructor(props) {
    super(props);
    this.state = props;
  }

  render() {
    return (
      <div className="categories">
        <div className="title">Успеваемость по категориям</div>
        <div className="rounds">
          <Circle index={0} percent={67} id={1} category='Игрушки'/>
          <Circle index={1} percent={88} id={2} category='Предметы'/>
          <Circle index={2} percent={95} id={3} category='Места'/>
        </div>
      </div>
    )
  }
}


Level.propTypes = {

};

import './index.css';
import React from 'react';
import ReactDOM from 'react-dom';
import Aside from './components/aside';
import UserImage from './components/userImage';
import Profile from './components/profile';
import Categories from './components/categories';
import Vocabulary from './components/vocabulary';





class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isUpperActive: true,
      upper: true,
      lower: false,
    }
  }

  click = (isUpperActive) => {
    this.setState({isUpperActive: isUpperActive});

  }
  componentDidMount() {
    if(this.state.isUpperActive) {
      this.setState({lower: false, upper: true});
      return;
    }
    this.setState({lower: true, upper: false});
  }

  render () {
    return (
      <div className="wrapper">
        <Aside upper={this.state.upper} lower={this.state.lower} click={this.click}/>
        <div className="content">
          <div className="mainInfo">
            <UserImage path='src/img/user.png' />
            <Profile points="89" prevLvl="1" time="1:23:06" tochnost="67" amount="56" name="Мишка Заплаткин"/>
          </div>
          {this.state.isUpperActive && <Categories />}
          {!this.state.isUpperActive && <Vocabulary />}
        </div>
      </div>
    )
  }
}



ReactDOM.render(
    <App />,
    document.getElementById('app')
)

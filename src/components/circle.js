import React, { Component } from 'react';
import PropTypes from 'prop-types';


export default class Level extends Component {
  constructor(props) {
    super(props);
    this.state = props;
  }

  draw = (percentsLoaded) => {
    const canvas = document.createElement('canvas');
    canvas.id = this.state.id;
    const canv = document.getElementById(this.state.id);
    this.drawCircle(percentsLoaded, canvas)
    if(!document.querySelectorAll(".canvas")[this.state.index]) {
      return;
    }
    document.querySelectorAll(".canvas")[this.state.index].replaceChild(canvas, canv);

    const div = document.createElement('div');
    div.className = "canv";
    div.innerHTML = `<div class="text">${this.state.category}</div><div class="percent">${Math.floor(percentsLoaded/2)}%</div>`;
    const old = document.getElementsByClassName("canv");
    document.querySelectorAll(".canvas")[this.state.index].replaceChild(div, old[this.state.index]);
    if(percentsLoaded === this.state.percent * 2) {
      return;
    }
    setTimeout(() => {this.draw(percentsLoaded + 1)}, 10);
  }

  drawCircle(percent, canvas) {
    canvas.width="400";
    canvas.height="400";
    let ctx = canvas.getContext('2d');
    ctx.lineWidth = 15; // толщина линии
    var gradient = ctx.createLinearGradient(0, 0, 400, 0);

  // Добавляем два цвета
    gradient.addColorStop(0, "#1be5f7");
    gradient.addColorStop(1, "#1444a4");
    const pi = Math.PI;
    let deg = -pi/2;
    ctx.strokeStyle = gradient;
    ctx.arc(140,140,120, -pi/2, deg+percent*(pi / 100), false);
    ctx.stroke();
  }

  componentDidMount() {
    this.draw(0);
  }

  render() {
    return (
      <div className="canvas">
        <div className="canv">Категория</div>
        <canvas id={this.state.id}></canvas>
      </div>
    )
  }
}


Level.propTypes = {

};

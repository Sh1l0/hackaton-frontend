import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class Info extends Component {
  constructor(props) {
    super(props);
    this.state = {
      path: this.props.path,
      alt: this.props.alt,
      data: this.props.data,
      children: this.props.children
    }
  }

  componentDidMount() {
    if(this.state.alt === 'Точность') {
      const data = this.state.data + '%';
      this.setState({data: data});
    }
  }

  render() {
    return (
      <div className="info">
        <div className="img"><img src={this.state.path} alt={this.state.alt} /></div>
        <div className="text">{this.state.children}</div>
        <div className="percent">{this.state.data}</div>
      </div>
    )
  }
}


Info.propTypes = {
  path: PropTypes.string,
  alt: PropTypes.string,
  children: PropTypes.string,
  data: PropTypes.string,
};
